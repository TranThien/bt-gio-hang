import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class RenderListShoe extends Component {
  renderShoe = () => {
    return this.props.listShoe.map((item) => {
      return (
        <ItemShoe
          key={item.id}
          data={item}
          handleAddToCart={this.props.handleAddToCart}
          handleDetailShoe={this.props.handleDetailShoe}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderShoe()}</div>;
  }
}
