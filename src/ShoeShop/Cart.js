import React, { Component } from "react";

export default class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((cart) => {
      return (
        <tr>
          <td>{cart.id}</td>

          <td>{cart.name}</td>
          <td>{(cart.price * cart.number).toLocaleString()}$</td>
          <td>
            <img src={cart.image} alt="" style={{ width: "50px" }} />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleReduceQuantity(cart.id);
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-3"> {cart.number}</span>
            <button
              onClick={() => {
                this.props.handleRaiseQuantity(cart.id);
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleRemoveItemCart(cart.id);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    let total = this.props.cart.reduce((cart, cur) => {
      return cart + +cur.price * +cur.number;
    }, 0);
    console.log(total);
    return (
      <div>
        <div
          className="modal fade"
          id="modelCart"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div
              className="modal-content"
              style={{ maxWidth: "900px", width: "900px" }}
            >
              <div className="modal-header">
                <h5 className="modal-title">Modal title</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="container table">
                  <thead>
                    <tr>
                      <td>Id</td>
                      <td>Name</td>
                      <td>Price</td>
                      <td>Image</td>
                      <td>Quantity</td>
                    </tr>
                  </thead>
                  <tbody>{this.renderCart()}</tbody>
                </table>
                <div className="text-right text-info">
                  Tổng tiền :
                  <span class="text-success">{total.toLocaleString()}</span> $
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Thanh Toán
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
