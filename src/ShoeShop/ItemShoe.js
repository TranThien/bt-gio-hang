import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    const data = this.props.data;
    const { name, price, image } = data;
    console.log(this.props.data);

    return (
      <div className="col-4 mt-3">
        <div className="card h-100">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">Tên SP: {name}</h5>
            <h3 className="card-title">Giá :{price}</h3>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleDetailShoe(data);
              }}
              className="btn btn-primary mr-2 mb-3"
              data-toggle="modal"
              data-target="#modelId"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(data);
              }}
              className="btn btn-success  mb-3"
            >
              Thêm
            </button>
          </div>
        </div>
      </div>
    );
  }
}
