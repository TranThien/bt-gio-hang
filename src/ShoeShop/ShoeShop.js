import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./datashoe";
import DetailShoe from "./DetailShoe";
import RenderListShoe from "./RenderListShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: {},
    cart: [],
  };
  //   Xử lí onClick xem chi tiết
  handleDetailShoe = (shoe) => {
    //setState là callback
    this.setState({ detail: shoe });
  };
  // Xử lí onClick cho nút thêm
  handleAddToCart = (shoe) => {
    console.log("shoe", shoe);
    // từ cart cũ trong state clone thành mảng mới
    let cloneCart = [...this.state.cart];

    //TH1 : CHưa có sản phẩm trong giỏ hàng
    //tìm kiếm vị trí index
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // sản phẩm k có trong giỏ hàng thì thêm mới
      // nó là Object
      let cartItem = { ...shoe, number: 1 };
      //từ cái mảng push thêm Object cart item vào
      cloneCart.push(cartItem);
      // rồi set state
      this.setState({ cart: cloneCart });
    } else {
    }

    //TH2 : Sản phẩm đã có trong giỏ hàng
    // từ vị trí index đó . đếm trường number r tăng lên 1 đơn vị
    cloneCart[index].number++;
    this.setState({ cart: cloneCart });
  };
  // muốn xóa sp trong giỏ hàng cần có id của nó
  handleRemoveItemCart = (idItem) => {
    let newCart = [...this.state.cart];
    console.log("newCart: ", newCart);
    const index = newCart.findIndex((item) => {
      return item.id === idItem;
    });

    if (index == -1) {
      return;
    }

    newCart.splice(index, 1);

    // set state lại :
    this.setState({ cart: newCart });
  };
  handleRaiseQuantity = (id) => {
    let newCart = [...this.state.cart];
    const index = newCart.findIndex((item) => {
      return item.id === id;
    });
    if (index !== -1) {
      newCart[index].number++;
    }
    this.setState({ cart: newCart });
  };
  handleReduceQuantity = (id) => {
    let newCart = [...this.state.cart];
    const index = newCart.findIndex((item) => {
      return item.id === id;
    });
    if (index !== -1 && newCart[index].number > 1) {
      newCart[index].number--;
    } else {
    }
    this.setState({ cart: newCart });
  };
  render() {
    let quantity = this.state.cart.reduce((acc, cur) => {
      return acc + cur.number;
    }, 0);
    return (
      <div className="container py-5">
        <div className="text-right">
          <span
            className="text-info"
            data-toggle="modal"
            data-target="#modelCart"
            style={{ cursor: "pointer" }}
          >
            Giỏ hàng ({quantity.toLocaleString()})
          </span>
        </div>
        <DetailShoe detail={this.state.detail} />
        <Cart
          handleReduceQuantity={this.handleReduceQuantity}
          handleRaiseQuantity={this.handleRaiseQuantity}
          handleRemoveItemCart={this.handleRemoveItemCart}
          cart={this.state.cart}
        />
        {/* <div className="row">
          {this.renderShoe()}
        </div> */}
        <RenderListShoe
          handleAddToCart={this.handleAddToCart}
          listShoe={this.state.shoeArr}
          handleDetailShoe={this.handleDetailShoe}
        />
      </div>
    );
  }
}
