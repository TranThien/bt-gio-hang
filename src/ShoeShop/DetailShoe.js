import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    const { name, alias, price, description, quantity, image } =
      this.props.detail;
    console.log(this.props.detail);
    return (
      <div className="container">
        <div>
          {/* Button trigger modal */}
          {/* <button
            type="button"
            className="btn btn-primary btn-lg"
            data-toggle="modal"
            data-target="#modelId"
          >
            Launch
          </button> */}
          {/* Modal */}
          <div
            className="modal fade"
            id="modelId"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="modelTitleId"
            aria-hidden="true"
          >
            <div className="modal-dialog" role="document">
              <div
                className="modal-content"
                style={{ maxWidth: "700px", width: "700px" }}
              >
                <div className="modal-header">
                  <h5 className="modal-title">Thông tin chi tiết</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body ">
                  <div className="row">
                    <img
                      src={image}
                      alt=""
                      className="col-4"
                      style={{ width: "150px" }}
                    />
                    <div className="col-8 text-left">
                      <h3>Tên sản phẩm : {name}</h3>
                      <h2>Hãng :{alias}</h2>
                      <h4>Giá : {price}</h4>

                      <p>Giới thiệu : {description}</p>
                      <h4>Số lượng còn lại : {quantity}</h4>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Close
                  </button>
                  {/* <button type="button" className="btn btn-primary">
                    Save
                  </button> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
